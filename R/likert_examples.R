
#' Quick likert plot
#'
#' \code{qplot} is a convenient wrapper for creating a set of different archetypes of likert plots.
#' It's great for a first introduction do likert plots,
#' but I highly recommend to dive into \code{\link{geom_likert}}() to finely adjust complex graphics.
#'
#' @aliases gglikert.type.1 gglikert.type.2 gglikert.type.3 gglikert.type.4 gglikert.type.5 gglikert.type.6
#'
#' @param data Default dataset to use for plot.
#' @param item The variable identifying the item. Unquoted.
#' @param response The variable identifying the response. Unquoted.
#' @param facet The (optional) variable identifying subgroup. Used by faceted archetypes. Unquoted.
#' @param archetype The type of plot to produce. See examples.
#' @param expose Should the result expose the ggplot2 code ?
#'
#' @seealso \code{\link[ggplot2]{qplot}}
#' @export
#'
#' @examples
#' library(dplyr)
#' library(stringr)
#' library(purrr)
#' set.seed(42)
#' temp_wide <-
#'   str_c("Q", 1:8) %>%
#'   set_names(., .) %>%
#'   map_dfc(~sample(letters[1:7], 60,
#'                   prob = c(1:7)^{sample(1:8, 1) %>% {(. - 4)/2}},
#'                   replace = TRUE) %>% ordered) %>%
#'   mutate_all(~factor(., levels =letters[1:7], ordered = TRUE)) %>%
#'   mutate(group = sample(c("A", "B", "C"), nrow(.), TRUE)) %>%
#'   mutate(id = 1:nrow(.))
#'
#' temp_long <-
#'   temp_wide %>%
#'   tidyr::gather("item", "response", -group, -id, factor_key = TRUE) %>%
#'   mutate(
#'     group_uneven =
#'       sample(c("A", "a"), nrow(.), replace = TRUE, prob = c(0.7,0.3)),
#'
#'     response_na = response %>%
#'       if_else(runif (.) < 0.15, NA_character_, .),
#'
#'     response_na2 = response %>%
#'       if_else(
#'         runif (.) < 0.10 & (as.numeric(str_remove(item, "Q")) %% 2) == 0,
#'         NA_character_, .)
#'   )
#'
#' temp_long %>% gglikert(item, response_na, archetype = "type 1")
#' temp_long %>% gglikert(item, response_na, archetype = "type 2")
#' temp_long %>% gglikert(item, response_na2, archetype = "type 3")
#' temp_long %>% gglikert(item, response_na2, archetype = "type 4")
#' temp_long %>% gglikert(item, response_na, facet = group_uneven, archetype = "type 5")
#' temp_long %>% gglikert(item = group, response_na, facet = item, archetype = "type 6")
#'
#' gglikert(expose = TRUE, archetype = "type 5")

gglikert <- function(data = NULL, item = NULL, response = NULL, facet = NULL,
                    archetype = c("type 1", "type 2", "type 3", "type 4", "type 5", "type 6"),
                    expose = FALSE){

  archetype <- match.arg(archetype)
  what <- str_c("gglikert.", str_replace(archetype, " ", "\\."))

  if (expose) get(what) else {
  do.call(
    what = what,
    args = list(data = data, item = enquo(item), response = enquo(response), facet = enquo(facet))
  )
  }
}

gglikert.type.1 <-
  function(data, item, response, facet = NULL){
    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      geom_likert(aes(fill = {{response}})) +
      geom_likert_text(marginal = TRUE) +
      scale_y_continuous(expand = c(.15, 0), breaks = (-5:5) / 5) +
      coord_flip() +
      scale_fill_likert() +
      theme_bw() +
      labs(y = "proportion of respondents")
  }
gglikert.type.2 <-
  function(data, item, response, facet = NULL){
    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      geom_likert(
        aes(fill = {{response}}),
        graph.type = "full_width",
        x.order = c("identity", "rev")
      ) +
      geom_likert_label(
        graph.type = "full_width",
        x.order = c("identity", "rev"),
        label.format =
          ~ ifelse(. > .1, str_c(round(100 * ., 0), " %"), NA_character_)
      ) +
      scale_y_continuous(breaks = (0:5) / 5) +
      coord_flip() +
      scale_fill_likert() +
      theme_bw() +
      labs(y = "proportion of respondents")
  }
gglikert.type.3 <-
  function(data, item, response, facet = NULL){

    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      stat_likert_double(
        aes(fill = {{response}}),
        graph.type = "full_width",
        na.position = "left",
        strict.full.width = FALSE,
        x.order = "na_count",
        geom_text = "LabelLikert",
        marginal = TRUE,
        show.na = TRUE,
        share.neutral = TRUE
      ) +
      scale_y_continuous(breaks = (0:5) / 5, expand = c(.1, .1)) +
      coord_flip() +
      scale_fill_likert() +
      theme_bw() +
      labs(y = "proportion of respondents")
  }
gglikert.type.4 <-
  function(data, item, response, facet = NULL){
    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      geom_likert(
        aes(fill = {{response}}, y = stat(.data$count)),
        graph.type = "full_width",
        na.position = "left",
        strict.full.width = FALSE,
        x.order = "na_count"
      ) +
      geom_likert_text(
        aes(
          y = stat(.data$count), label = stat(.data$count),
          vjust = stat(.data$hjust), hjust = stat(.data$vjust)
          ),
        marginal = TRUE,
        graph.type = "full_width",
        na.position = "left",
        show.na = TRUE,
        share.neutral = TRUE,
        strict.full.width = FALSE,
        x.order = c("na_count", "rev")
      ) +
      geom_likert_text(
        aes( y = stat(.data$count), label = stat(.data$count)),
        graph.type = "full_width",
        na.position = "left",
        strict.full.width = FALSE,
        x.order = c("na_count", "rev")
      ) +
      scale_y_continuous(expand = c(.1, .1)) +
      scale_fill_likert() +
      theme_bw() +
      labs(y = "number of respondents")
  }
gglikert.type.5 <-
  function(data, item, response, facet = NULL){
    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      geom_likert(aes(fill = {{response}}, y = stat(.data$count))) +
      geom_likert_text(marginal = TRUE) +
      scale_y_continuous(expand = c(.30, .30)) +
      scale_fill_likert() +
      coord_flip() +
      theme_bw() +
      labs(y = "number of respondents") +
      facet_grid(cols = vars({{facet}}))
  }
gglikert.type.6 <-
  function(data, item, response, facet = NULL){
    data %>%
      ggplot() +
      aes(item = {{item}}, response = {{response}}) +
      geom_hline(yintercept = 0) +
      geom_likert(
        aes(fill = {{response}}),
        x.order = c("identity", "rev"),
        item.neutral.point = 2.5,
        na.rm = TRUE
      ) +
      scale_y_continuous(breaks = (-5:5) / 5) +
      scale_fill_likert(center.point = 2.5) +
      coord_flip() +
      theme_bw() +
      labs(y = "proportion of respondents", x = "group") +
      facet_grid(rows = vars({{facet}}))
  }


