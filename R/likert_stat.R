
# helpers ----

#' Usefull caracteristics of a likert item
#'
#' Get some usefull caracteristics of a likert item
#'
#' @param factor Vector, coerced as factor if needed
#' @param neutral.point Optionnal, used to set the neutral
#' point if the item is asymetric.
#'
#' If integer, the neutral point will be the \emph{n}th value. Else, it
#' will be in the interval between the previous and next values.
#'
#' @return A list with :
#'   \describe{
#'     \item{neutral_n}{The numeric position of the neutral value.}
#'     \item{minus}{Vector of label of responses strictly inferior to the neutral value.}
#'     \item{plus}{Vector of label of responses strictly superior to the neutral value}
#'     \item{neutral}{Label of the neutral value, if exist, or NA_character_}
#'   }
#' @keywords internal
#'
#' @examples
#' # item_caract(letters[1:7])
#' # item_caract(letters[1:7], neutral.point = 2.5)
item_caract <-
  function(factor, neutral.point = NULL) {

    factor = as.ordered(factor)

    if(is.character(neutral.point) || is.factor(neutral.point)){
      neutral = match.arg(as.character(neutral.point), levels(factor))
      neutral_n = which(levels(factor) == neutral)

    } else {

      if(is.null(neutral.point)){
        neutral_n = factor %>% nlevels() %>% {(. + 1) / 2}
      } else if(is.numeric(neutral.point)){
        neutral_n = neutral.point
      }

      neutral = if_else((neutral_n %% 1) == 0, levels(factor)[neutral_n], NA_character_)

    }

    minus = factor %>% levels() %>% .[seq_along(.) < neutral_n]

    plus = factor %>% levels() %>% .[seq_along(.) > neutral_n]


    list(neutral_n = neutral_n, minus = minus, plus = plus, neutral = neutral)

  }



#' StatLikert missing repositionner helper
#'
#' Rewrite \code{ymin} and \code{ymax} values in \code{data} according
#' to the mode in \code{na.position}.
#'
#' @param data A dataframe containing the variables
#' \code{item}, \code{response}, \code{count}, \code{prop},
#'  \code{group} and  \code{item.position}
#' (the relation to the neutral point).
#' @param na.position One of
#' "right" (do nothing, missing stay in the right of the plot),
#' "left" (replace missing count as negative, left in plot),
#' "none" (remove row with missing \code{response}) and
#' "split" (split missing responses count in half between
#' "right" positive position, and "left" negative position,
#' correcting \code{count} and \code{prop} to display the correct label)
#'
#' @keywords internal
#'
#' @examples
#' library(tibble)
#' temp <-
#'   tibble::tribble(
#'     ~PANEL, ~item, ~response, ~count, ~prop, ~item.position, ~group,
#'          1,   "A",      "no",      4,   0.4,        "minus",     1L,
#'          1,   "A", "neutral",      2,   0.2,       "neutral",     2L,
#'          1,   "A",     "yes",      4,   0.4,         "plus",     3L,
#'          1,   "B",      "no",      2,   0.2,        "minus",     1L,
#'          1,   "B", "neutral",      1,   0.1,       "neutral",     2L,
#'          1,   "B",     "yes",      6,   0.6,         "plus",     3L,
#'          1,   "B",        NA,      1,   0.1,         "plus",     4L
#'     )
#' # split_move_remove_na(temp, na.position = "left")
#' # split_move_remove_na(temp, na.position = "none")
#' # split_move_remove_na(temp, na.position = "split")
#'
split_move_remove_na <- # na_work ?
  function(data, na.position){

    if(na.position == "split"){
      data <-
        data %>%
        mutate(
          count = ifelse(is.na(.data$response), .data$count/2, .data$count),
          prop = ifelse(is.na(.data$response), .data$prop/2, .data$prop)
        ) %>%
        bind_rows(
          filter(., is.na(.data$response)) %>%
            mutate(item.position  = "minus", group = 0)
        )
    } else if(na.position == "left"){
      data <-
        data %>%
        mutate(
          item.position = if_else(is.na(.data$response), "minus",
                                  .data$item.position)
          # group = if_else(is.na(response), 0, group)
          )

      data$group[is.na(data$response)] <- 0

    } else if(na.position == "none"){
      data <-
        data %>%
        filter(! is.na(.data$response))
    } # else if(na.position == "right") data <- data

    data %>%
      arrange(.data$item, .data$group)
  }



#' StatLikert reordering helper
#'
#' Reorder the factor \code{item} in \code{data} according
#' to the mode in \code{x.order}
#'
#'
#' @param data A dataframe containing the variables
#' \code{PANEL}, \code{item}, \code{response}, \code{count}, and
#'  \code{item.position} (the relation to the neutral point).
#' @param x.order A character vector controling the type of ordering.
#' Should have one of "identity", "median_response" or "na_count" and
#' an optional "rev".
#'
#' @keywords internal
#'
#' @examples
#' library(dplyr)
#' temp <-
#'   tibble::tribble(
#'     ~PANEL, ~item, ~response, ~count, ~prop, ~item.position, ~group,
#'          1,   "A",      "no",      4,   0.4,        "minus",     1L,
#'          1,   "A", "neutral",      2,   0.2,       "neutral",     2L,
#'          1,   "A",     "yes",      4,   0.4,         "plus",     3L,
#'          1,   "B",      "no",      2,   0.2,        "minus",     1L,
#'          1,   "B", "neutral",      1,   0.1,       "neutral",     2L,
#'          1,   "B",     "yes",      6,   0.6,         "plus",     3L,
#'          1,   "B",        NA,      1,   0.1,         "plus",     4L
#'     )
#'
#' # order_items(temp, x.order = "identity") %>% pull(item)
#' # order_items(temp, x.order = c("median_response", "rev")) %>% pull(item)
#' # order_items(temp, x.order = "na_count") %>% pull(item)
#'
order_items <-
  function(data, x.order) {
    if ("identity" %in% x.order) {
      data <-
        data %>%
        mutate(item =  ordered(.data$item))

    } else if ("median_response" %in% x.order) {
      data <-
        data %>%
        group_by(.data$PANEL, .data$item) %>%
        mutate(
          shift =
            case_when(
              item.position == "minus" ~ .data$prop,
              item.position == "neutral" ~ .data$prop / 2,
              TRUE ~ 0
            ) %>% sum()
        ) %>%
        ungroup() %>%
        mutate(item = fct_reorder(.data$item, .data$shift, .desc = TRUE)) %>%
        select(-.data$shift)

    } else if ("na_count" %in% x.order) {
      data <-
        data %>%
        mutate(
          item = .data$item %>% fct_rev() %>%
            fct_reorder2(
              .f = ., .x = .data$response, .y = .data$count,
              .fun = function(.x, .y) c(.y[is.na(.x)], 1)[1],
              .desc = FALSE )
        )
    }

    if ("rev" %in% x.order) {
      data <-
        data %>%
        mutate(item = .data$item %>% fct_rev())
    }
    data %>% mutate(item = ordered(.data$item))
  }

# Stat ----

StatLikert <-
  {ggproto(
    `_class` = "StatLikert", `_inherit` = Stat,

    required_aes = c("item", "response"),

    default_aes =
      aes(x = stat(item), y = stat(prop),
          hjust = stat(hjust), vjust = stat(vjust),
          label = stat(prop)),

    extra_params = c("na.rm", "item.neutral.point"),

    setup_params =
      function(data, params, quiet = FALSE){

        params <-
          c(params,
            item_caract(data$response, neutral.point = params$item.neutral.point)
          )

        if(is.null(params$graph.type)) {
          params$graph.type <- "center"
          # ggplot2:::message_wrap('`stat_likert()` using `graph.type = "',params$graph.type,'"`')
        } else{
          params$graph.type <-
            match.arg(params$graph.type,
                      c("full_width", "center", "diverging stacked bar")) %>%
            recode("diverging stacked bar" = "center")
        }


        if (is.null(params$strict.full.width)){
          params$strict.full.width <- TRUE
        }

        if(is.null(params$x.order)){
          params$x.order <-
            case_when(
              params$graph.type == "full_width" ~ "identity",
              params$graph.type == "center" ~ "median_response",
              TRUE ~ "na_count"
            )
          # if(params$x.order != "identity"){
          #   ggplot2:::message_wrap('`stat_likert()` using `x.order = "', params$x.order,'"`')
          # }
        } else {
          params$x.order <-
            match.arg(params$x.order,
                      c("identity", "median_response", "na_count", "rev"),
                      several.ok = TRUE)
        }




        if(is.null(params$na.position)) {
          params$na.position <-
            case_when(
              params$graph.type == "full_width" ~ "right",
              params$graph.type == "center" ~ "split",
              TRUE ~ "none"
            )
          # if(any(is.na(data$response))){
          #   ggplot2:::message_wrap('`stat_likert()` using `na.position = "', params$na.position,'"`')
          # }
        } else {
          params$na.position <-
            match.arg(params$na.position, c("left", "right", "split", "none"))
        }


        params

      },


    compute_layer =
      function (self, data, params, layout) {

        # copypasted internals from ggplot2 ----
        check_required_aesthetics <-
          function (required, present, name) {
            missing_aes <- setdiff(required, present)
            if (length(missing_aes) == 0)
              return()
            stop(name, " requires the following missing aesthetics: ",
                 paste(missing_aes, collapse = ", "), call. = FALSE)
          }

        snake_class <- function(x){
          x <- gsub("([A-Za-z])([A-Z])([a-z])", "\\1_\\2\\3", x)
          x <- gsub(".", "_", x, fixed = TRUE)
          x <- gsub("([a-z])([A-Z])", "\\1_\\2", x)
          stringr::str_to_lower(x)
        }
        # ----

        check_required_aesthetics(
          required = self$required_aes,  present = c(names(data), names(params)),
          snake_class(self))

        if(params$na.rm){
          data <-
        #     ggplot2:::remove_missing(
        #     data, params$na.rm, c(self$required_aes, self$non_missing_aes),
        #     snake_class(self), finite = FALSE) # finite remove all non-number
          data %>%
          filter_at(
            vars(one_of(c(self$required_aes, self$non_missing_aes))),
            ~!is.na(.)) %>%
          {if(nrow(.) == nrow(data)) . else {
            warning("Removed ", nrow(.) - nrow(data), " rows containing ",
                         "missing", " values.")
            .}}
        }


        params <- params[intersect(names(params), self$parameters())]

        args <- c(list(data = quote(data), scales = quote(scales)), params)

        data <-
          # ggplot2:::dapply(
          #   data, "PANEL",
          data %>% split(.$PANEL) %>%
          purrr::map_dfr(
            function(data) {
              scales <- layout$get_scales(data$PANEL[1])
              tryCatch(
                do.call(self$compute_panel, args),
                error = function(e) {
                  warning("Computation failed in `", snake_class(self),
                          "()`:\n", e$message, call. = FALSE)
                  # ggplot2:::new_data_frame()
                  data.frame()
                })
            })


        # item order
        data <-
          data %>%
          order_items(x.order = params$x.order) %>%
          mutate(hjust = "hjust", vjust = "vjust")


        data
      },

    compute_panel =
      function(data, scales, x.order,
               graph.type, na.position, strict.full.width,
               minus, neutral) {

        data <-
          data %>%
          # should not lose any of these as any can be needed in other parts
          # response => group (mais en numeric et NA => un group)
          group_by_all() %>% count(name = "count") %>%
          group_by(item) %>%
          mutate(
            prop = count / sum(count),
            item.position =
              case_when(
                response %in% minus ~ "minus",
                response == neutral ~ "neutral",
                TRUE ~ "plus")
          ) %>% ungroup


        # na.position
        data <-
          data %>%
          split_move_remove_na(na.position = na.position)



        if(graph.type == "center"){

          data <- data %>%
            mutate(y.position = item.position,
                   marginal.position = y.position)

        } else if(graph.type == "full_width" & ! strict.full.width){

          data <-
            data %>%
            mutate(y.position =
                     if_else(is.na(response) &
                               item.position == "minus",
                             "minus", "plus"),
                   marginal.position = y.position)

        } else {

          data <- data %>% mutate(y.position = "plus",
                                  marginal.position = item.position)

        }

        data
      }
  )}


#
# Geom ----

get_shift <- function(data){
  data %>%
  group_by(.data$PANEL, .data$x) %>%
    mutate(y_val = as.double(.data$y),
           y = cumsum(.data$y_val) - .data$y_val/2,
           shift = case_when(
             y.position == "minus" ~ .data$y_val,
             y.position == "neutral" ~ .data$y_val/2,
             TRUE ~ 0) %>% sum()) %>%
    ungroup()
}

GeomLikert <-
  {ggproto(
    `_class` = "GeomLikert", `_inherit` = GeomCol,

    setup_data =
      function (data, params) {


        if(is.null(params$width)) params$width <- 0.9

        data$width <- data$width %||% params$width

        data %>%
          get_shift() %>%
          mutate(y = y - shift,
                 # x = as.numeric(x),
                 ymin = y - y_val/2, ymax = y + y_val/2,
                 xmin = as.numeric(x) - width/2, xmax = as.numeric(x) + width/2,
                 width = NULL,
                 hjust = coalesce(quietly(as.numeric)(hjust)$result, 0.5),
                 vjust = coalesce(quietly(as.numeric)(vjust)$result, 0.5)
          )
      }


    # draw_panel : inherit of a ggproto_parent(GeomRect, self)
  )}
#

GeomTextLikert <-
  {ggproto(
    `_class` = "GeomTextLikert", `_inherit` = GeomText,

    extra_params = c("na.rm", "label.format", "marginal",
                     "share.neutral", "show.na"),

    setup_data = function (data, params){

      # label.format
      if (! is.null(params$label.format)) {

        params$label.format <- as_mapper(params$label.format)

      } else if ((! is.numeric(data$label)) ||
                 any(abs(data$label) >= 2)) { # == y = stat(count)

        params$label.format <- as_mapper(~map(., format) %>% format())

      } else {

        params$label.format <- as_mapper(
          ~map_chr(., ~format(100*., digits = 2)) %>% str_c(" %") %>%
            format(.))

      }

      # marginal

      if (is.null(params$marginal)) params$marginal <- FALSE
      if (is.null(params$share.neutral)) params$share.neutral <- FALSE
      if (is.null(params$show.na)) params$show.na <- FALSE

      data <-
        data %>%
        get_shift() %>%
        mutate(y = y - shift)

      if (params$marginal) {

        if (params$share.neutral){
          data <-
            data %>%
            mutate(
              y_val = if_else(item.position == "neutral", y_val/2, as.double(y_val)),
              item.position = item.position %>%{
                if_else(. == "neutral", false = as.list(.),
                        true = list(c("minus", "plus"))
                )}) %>%
            unnest()
        }

        data <-
          data %>%
          filter((!is.na(response)) | params$show.na) %>%
          group_by(PANEL, x, marginal.position) %>%
          summarise_all(list) %>%
          ungroup %>%
          mutate(
            label =
              map_dbl(label, ~ sum(as.numeric(.)))
          ) %>%
          left_join(
            tibble(
              marginal.position = c("minus", "neutral", "plus"),
              y_ = c(-Inf, NA, Inf),
              yjust = c(-0.3, 0.5, 1.3)
            ),
            by = c("marginal.position")
          )

        data <-
          data %>%
          mutate_if(is.list, ~map(.,1) %>% unlist) %>%
          mutate(y  = coalesce(y_, y)) %>%
          mutate_at(
            vars(hjust, vjust),
            ~case_when(
              . == "hjust" ~ yjust,
              . == "vjust" ~ 0.5,
              TRUE ~ quietly(as.numeric)(.)$result %||% 0.5)
          ) %>%
          group_by(PANEL, x) %>%
          mutate(group = row_number())

      }

      data %>%
        mutate(
          hjust = coalesce(quietly(as.numeric)(hjust)$result, 0.5),
          vjust = coalesce(quietly(as.numeric)(vjust)$result, 0.5),
          label = label %>% params$label.format())
    }
  )}

#
GeomLabelLikert <-
  {ggproto(
    `_class` = "GeomLabelLikert", `_inherit` = GeomLabel,

    extra_params = GeomTextLikert$extra_params,

    setup_data = GeomTextLikert$setup_data
      # function (data, params){
      #   GeomTextLikert$setup_data(data, params)
      # }
  )}
#
