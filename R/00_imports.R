#' @import ggplot2
#' @importFrom rlang .data
#' @importFrom dplyr %>%
#' @importFrom dplyr arrange
#' @importFrom dplyr as_tibble
#' @importFrom dplyr bind_rows
#' @importFrom dplyr case_when
#' @importFrom rlang enquo
#' @importFrom forcats fct_reorder
#' @importFrom forcats fct_reorder2
#' @importFrom forcats fct_rev
#' @importFrom dplyr filter
#' @importFrom dplyr group_by
#' @importFrom dplyr if_else
#' @importFrom purrr map_dfr
#' @importFrom purrr map2_lgl
#' @importFrom dplyr mutate
#' @importFrom dplyr n
#' @importFrom purrr quietly
#' @importFrom rlang quo_name
#' @importFrom dplyr recode
#' @importFrom dplyr row_number
#' @importFrom dplyr select
#' @importFrom rlang set_names
#' @importFrom dplyr slice
#' @importFrom stringr str_c
#' @importFrom stringr str_replace
#' @importFrom dplyr summarise
#' @importFrom dplyr ungroup
#' @importFrom tidyr unnest
NULL

utils::globalVariables(".")
